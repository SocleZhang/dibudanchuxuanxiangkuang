//
//  NSObject+DIY.m
//  NewsTestDemo
//
//  Created by yzrs on 16/11/22.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import "NSObject+DIY.h"
#import <objc/runtime.h>

@implementation NSObject (DIY)

-(NSString *)afy_string
{
    id obj = self;
    if (!self || [self isKindOfClass:[NSNull class]]) {
        obj = @"<null>";
    }
    return [NSString stringWithFormat:@"%@", obj];
}


- (NSDictionary *)getAllPropertiesAndVaules
{
    NSMutableDictionary *props = [NSMutableDictionary dictionary];
    unsigned int outCount, i;
    objc_property_t *properties =class_copyPropertyList([self class], &outCount);
    for (i = 0; i<outCount; i++)
    {
        objc_property_t property = properties[i];
        const char* char_f =property_getName(property);
        NSString *propertyName = [NSString stringWithUTF8String:char_f];
        id propertyValue = [self valueForKey:(NSString *)propertyName];
        if (propertyValue) [props setObject:propertyValue forKey:propertyName];
    }
    free(properties);
    return props;
}

-(void)callServiceWithTel:(NSString *)telString withSuperView:(UIView *)superView
{
    
    NSString *phoneNumber = [NSString stringWithFormat:@"tel://%@",telString];
    
    UIWebView *telWebView = [[UIWebView alloc]init];
    
    [superView addSubview:telWebView];
    
    [telWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:phoneNumber]]];
}

@end
