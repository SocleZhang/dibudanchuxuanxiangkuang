//
//  UIView+DIY.m
//  NewsTestDemo
//
//  Created by yzrs on 16/11/21.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import "UIView+DIY.h"
#import "AFYMethod.h"

@implementation UIView (DIY)

/*
 afy_topY;       //frame.origin.y
 afy_leftX;      //frame.origin.x
 afy_bottomY;    //frame.origin.y + frame.size.height
 afy_rightX;     //frame.origin.x + frame.size.width
 afy_width;      //frame.size.width
 afy_height;     //frame.size.height
 afy_centerX;    //frame.center.x
 afy_centerY;    //frame.center.y
 afy_origin;     //frame.origin
 afy_size;       //frame.size
 */
#pragma mark - 重写 新 属性
//1.0 frame.origin.y
-(CGFloat)afy_topY
{
    return self.afy_origin.y;
}
-(void)setAfy_topY:(CGFloat)afy_topY
{
    CGPoint origin  = self.afy_origin;
    origin.y        = afy_topY;
    self.afy_origin = origin;
}

//2.0 //frame.origin.x
-(CGFloat)afy_leftX
{
    return self.afy_origin.x;
}

-(void)setAfy_leftX:(CGFloat)afy_leftX
{
    CGPoint origin  = self.afy_origin;
    origin.x        = afy_leftX;
    self.afy_origin = origin;
}

//3.0 //frame.origin.y + frame.size.height
-(CGFloat)afy_bottomY
{
    return self.afy_topY + self.afy_height;
}

-(void)setAfy_bottomY:(CGFloat)afy_bottomY
{
    CGPoint origin  = self.afy_origin;
    origin.y = afy_bottomY - self.afy_height;
    self.afy_origin = origin;
}

//4.0 //frame.origin.x + frame.size.width
-(CGFloat)afy_rightX
{
    return self.afy_leftX + self.afy_width;
}

-(void)setAfy_rightX:(CGFloat)afy_rightX
{
    CGPoint origin  = self.afy_origin;
    origin.x = afy_rightX - self.afy_width;
    self.afy_origin = origin;
}

//5.0 //frame.size.width
-(CGFloat)afy_width
{
    return self.afy_size.width;
}

-(void)setAfy_width:(CGFloat)afy_width
{
    CGSize size = self.afy_size;
    size.width  = afy_width;
    self.afy_size = size;
}

//6.0 //frame.size.height
-(CGFloat)afy_height
{
    return self.afy_size.height;
}

-(void)setAfy_height:(CGFloat)afy_height
{
    CGSize size = self.afy_size;
    size.height  = afy_height;
    self.afy_size = size;
}

//7.0 //frame.center.x
-(CGFloat)afy_centerX
{
    return self.center.x;
}

-(void)setAfy_centerX:(CGFloat)afy_centerX
{
    CGPoint center = self.center;
    center.x = afy_centerX;
    self.center = center;
}

//8.0 //frame.center.y
-(CGFloat)afy_centerY
{
    return self.center.y;
}

-(void)setAfy_centerY:(CGFloat)afy_centerY
{
    CGPoint center = self.center;
    center.y = afy_centerY;
    self.center = center;
}

//9.0 //frame.origin
-(CGPoint)afy_origin
{
    return self.frame.origin;
}

-(void)setAfy_origin:(CGPoint)afy_origin{
    CGRect frame = self.frame;
    frame.origin = afy_origin;
    self.frame   = frame;
}

//10.0 //frame.size
-(CGSize)afy_size
{
    return self.frame.size;
}

-(void)setAfy_size:(CGSize)afy_size{
    CGRect frame = self.frame;
    frame.size   = afy_size;
    self.frame   = frame;
}


#pragma mark - 快速初始化UIView 等
-(instancetype)setFrameBlock:(FrameBlock)frameBlock
{
    frameBlock(self);
    return self;
}

-(instancetype)addTag:(NSInteger)tag
{
    self.tag = tag;
    return self;
}

-(instancetype)addToTheSuperView:(UIView *)superView
{
    [superView addSubview:self];
    return self;
}

-(instancetype)addTapGuestureTarget:(id)target withSelector:(SEL)selector{
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:selector];
    self.userInteractionEnabled = YES;
    [self addGestureRecognizer:tap];
    
    return self;
}


+(instancetype)viewWithFrameBlock:(FrameBlock)frameBlock
{
    Class viewClass = [self class];
    return [[[viewClass alloc] init] setFrameBlock:frameBlock];
}

+(instancetype)viewWithSuperView:(UIView *)superView
{
    Class viewClass = [self class];
    UIView *view = [[viewClass alloc] init];
    [superView addSubview:view];
    return view;
}

+(instancetype)viewWithSuperView:(UIView *)superView andBgColor:(UIColor *)bgColor
{
    Class viewClass = [self class];
    UIView *view = [[viewClass alloc] init];
    view.backgroundColor = bgColor;
    return view;
}

+(instancetype)viewWithSuperView:(UIView *)superView andFrameBlock:(FrameBlock)frameBlock
{
    Class viewClass = [self class];
    UIView *view = [[viewClass alloc] init];
    [superView addSubview:view];
    return [view setFrameBlock:frameBlock];
}

+(instancetype)viewWithSuperView:(UIView *)superView andBgColor:(UIColor *)bgColor andFrameBlock:(FrameBlock)frameBlock
{
    Class viewClass = [self class];
    UIView *view = [[viewClass alloc] init];
    view.backgroundColor = bgColor;
    [superView addSubview:view];
    return [view setFrameBlock:frameBlock];
}

//添加线条  //默认为view的宽度  高度为0.5
#pragma mark - 添加线条
-(instancetype)addTopLine
{
    //添加线条
    [UIView viewWithSuperView:self andBgColor:DefaultColor andFrameBlock:^(UIView *view) {
        view.afy_leftX = 0;
        view.afy_topY  = 0;
        view.afy_width = self.afy_width;
        view.afy_height= 0.5;
    }];
    
    return self;
}
-(instancetype)addBottomLine
{
    //添加线条
    [UIView viewWithSuperView:self andBgColor:DefaultColor andFrameBlock:^(UIView *view) {
        view.afy_leftX = 0;
        view.afy_topY  = self.afy_height - 0.5;
        view.afy_width = self.afy_width;
        view.afy_height= 0.5;
    }];
    
    return self;
}

-(instancetype)addTopAndBottomLine
{
    return [[self addTopLine] addBottomLine];
}

-(instancetype)addTopLineWithColor:(UIColor *)color
{
    //添加线条
    [UIView viewWithSuperView:self andBgColor:color andFrameBlock:^(UIView *view) {
        view.afy_leftX = 0;
        view.afy_topY  = 0;
        view.afy_width = self.afy_width;
        view.afy_height= 0.5;
        view.backgroundColor = color;
    }];
    
    return self;
}
-(instancetype)addBottomLineWithColor:(UIColor *)color
{
    //添加线条
    [UIView viewWithSuperView:self andBgColor:color andFrameBlock:^(UIView *view) {
        view.afy_leftX = 0;
        view.afy_width = self.afy_width;
        view.afy_height= 0.5;
        view.afy_bottomY  = self.afy_height;
        view.backgroundColor = color;
    }];
    
    return self;
}
-(instancetype)addTopAndBottomLineWithColor:(UIColor *)color
{
    return [[self addBottomLineWithColor:color] addTopLineWithColor:color];
}

-(instancetype)addLineWithFrameBlock:(FrameBlock)frameBlock
{
    //添加线条
    [UIView viewWithSuperView:self andBgColor:DefaultColor andFrameBlock:frameBlock];
    
    return self;
}

-(instancetype)addLineWithColor:(UIColor *)color andFrameBlock:(FrameBlock)frameBlock
{
    //添加线条
    [UIView viewWithSuperView:self andBgColor:color andFrameBlock:frameBlock];
    
    return self;
}

@end
