//
//  UILabel+DIY.m
//  NewsTestDemo
//
//  Created by yzrs on 16/11/21.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import "UILabel+DIY.h"
#import "NSObject+DIY.h"

@implementation UILabel (DIY)

-(instancetype)labelWithColor:(UIColor *)color
{
    self.textColor = color;
    return self;
}
-(instancetype)labelWithFontSize:(CGFloat)fontSize
{
    self.font = [UIFont systemFontOfSize:fontSize];
    return self;
}
-(instancetype)labelWithBoldFontSize:(CGFloat)fontSize
{
    self.font = [UIFont boldSystemFontOfSize:fontSize];
    return self;
}
-(instancetype)labelWithText:(NSString *)text
{
    self.text = [text afy_string];
    [self sizeToFit];
    return self;
}
-(instancetype)labelWithSuperView:(UIView *)superView
{
    [superView addSubview:self];
    return self;
}

+(instancetype)labelWithFontSize:(CGFloat)fontSize andColor:(UIColor *)color andText:(NSString *)text
{
    UILabel *label = [[[self class] alloc] init];
    [[[label labelWithFontSize:fontSize] labelWithColor:color] labelWithText:text];
    return label;
}
+(instancetype)labelWithFontSize:(CGFloat)fontSize andColor:(UIColor *)color andText:(NSString *)text andSuperView:(UIView *)superView
{
    UILabel *label = [[[self class] alloc] init];
    [[[[label labelWithFontSize:fontSize] labelWithColor:color] labelWithText:text] labelWithSuperView:superView];
    return label;
}

+(instancetype)labelWithBoldFontSize:(CGFloat)fontSize andColor:(UIColor *)color andText:(NSString *)text
{
    UILabel *label = [[[self class] alloc] init];
    [[[label labelWithBoldFontSize:fontSize] labelWithColor:color] labelWithText:text];
    return label;
}
+(instancetype)labelWithBoldFontSize:(CGFloat)fontSize andColor:(UIColor *)color andText:(NSString *)text andSuperView:(UIView *)superView
{
    UILabel *label = [[[self class] alloc] init];
    [[[[label labelWithBoldFontSize:fontSize] labelWithColor:color] labelWithText:text] labelWithSuperView:superView];
    return label;
}

@end
