//
//  UITextField+DIY.m
//  NewsTestDemo
//
//  Created by yzrs on 16/11/22.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import "UITextField+DIY.h"

@implementation UITextField (DIY)

+(instancetype)textFieldWithStandFontSize:(CGFloat)size placeHolderText:(NSString *)text placeHolderColor:(UIColor *)color
{
    UITextField *tf = [[[self class] alloc]init];
    tf.font = FontCreate(size);
    
    NSDictionary *dicAttr= @{
                             NSFontAttributeName: tf.font,
                             NSForegroundColorAttributeName: color
                             };
    
    NSAttributedString *attriStr = [[NSAttributedString alloc]initWithString:text attributes:dicAttr];
    tf.attributedPlaceholder = attriStr;
    
    [tf sizeToFit];
    return tf;
}

+(instancetype)textFieldWithStandFontSize:(CGFloat)size placeHolderText:(NSString *)text placeHolderColor:(UIColor *)color superView:(UIView *)superView
{
    UITextField *tf = [UITextField textFieldWithStandFontSize:size placeHolderText:text placeHolderColor:color];
    [superView addSubview:tf];
    return tf;
}

@end
