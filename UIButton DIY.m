//
//  UIButton+DIY.m
//  NewsTestDemo
//
//  Created by yzrs on 16/11/22.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import "UIButton+DIY.h"
#import "AFYMethod.h"

@implementation UIButton (DIY)

//_.01 设置Title
-(instancetype)setButtonTitle:(NSString *)title withState:(UIControlState)state
{
    if (![title isKindOfClass:[NSString class]]){
        return self;
    }
    [self setTitle:title forState:state];
    return self;
}

//_.02 设置Image
-(instancetype)setButtonImageName:(NSString *)imageName withState:(UIControlState)state
{
    if (![imageName isKindOfClass:[NSString class]]){
        return self;
    }
    [self setImage:ImageCreate(imageName) forState:state];
    return self;
}

//_.03 设置BackgroundImage
-(instancetype)setButtonBackgroundImageName:(NSString *)backgroundImageName withState:(UIControlState)state
{
    if (![backgroundImageName isKindOfClass:[NSString class]]){
        return self;
    }
    [self setBackgroundImage:ImageCreate(backgroundImageName) forState:state];
    return self;
}

//_.04 设置TitleColor
-(instancetype)setButtonTitleColor:(UIColor *)titleColor withState:(UIControlState)state
{
    if (![titleColor isKindOfClass:[UIColor class]]){
        return self;
    }
    [self setTitleColor:titleColor forState:state];
    return self;
}

//_.05 设置BackgroundColor
-(instancetype)setButtonBackgroundColor:(UIColor *)backgroundColor withState:(UIControlState)state
{
    if (![backgroundColor isKindOfClass:[UIColor class]]){
        return self;
    }
    [self setBackgroundImage:[AFYMethod createImageWithColor:backgroundColor] forState:state];
    return self;
}

//0.01 设置titles, 默认第一个为normal 第二个为select
-(instancetype)setButtonTitles:(NSArray<NSString *> *)titleArray
{
    NSUInteger stateArray[2] = {UIControlStateNormal, UIControlStateSelected};
    if (titleArray.count) {
        for (int i=0; i<titleArray.count; i++) {
            [self setButtonTitle:[titleArray objectAtIndex:i] withState:*(stateArray + i)];
            if (i>=2) {
                break;
            }
        }
        [self sizeToFit];
    }
    return self;
}

//0.02 设置Images, 默认第一个为normal 第二个为select
-(instancetype)setButtonImageNames:(NSArray<NSString *> *)imageNames
{
    NSUInteger stateArray[2] = {UIControlStateNormal, UIControlStateSelected};
    if (imageNames.count) {
        for (int i=0; i<imageNames.count; i++) {
            [self setButtonImageName:[imageNames objectAtIndex:i] withState:*(stateArray + i)];
            if (i>=2) {
                break;
            }
        }
    }
    return self;
}

//0.03 设置BackgroundImages, 默认第一个为normal 第二个为select
-(instancetype)setButtonBackgroundImageNames:(NSArray<NSString *> *)backgroundImageNames
{
    NSUInteger stateArray[2] = {UIControlStateNormal, UIControlStateSelected};
    if (backgroundImageNames.count) {
        for (int i=0; i<backgroundImageNames.count; i++) {
            [self setButtonBackgroundImageName:[backgroundImageNames objectAtIndex:i] withState: *(stateArray + i)];
            if (i>=2) {
                break;
            }
        }
    }
    return self;
}

//0.04 设置BackgroundColor, 默认第一个为normal 第二个为select
-(instancetype)setButtonBackgroundColors:(NSArray<UIColor *> *)backgroundColors
{
    NSUInteger stateArray[2] = {UIControlStateNormal, UIControlStateSelected};
    if (backgroundColors.count) {
        for (int i=0; i<backgroundColors.count; i++) {
            [self setButtonBackgroundColor:[backgroundColors objectAtIndex:i] withState:*(stateArray + i)];
            if (i>=2) {
                break;
            }
        }
    }
    return self;
}

//0.05 设置TitleColor, 默认第一个为normal 第二个为select
-(instancetype)setButtonTitleColors:(NSArray<UIColor *> *)titleColors
{
    NSUInteger stateArray[2] = {UIControlStateNormal, UIControlStateSelected};
    if (titleColors.count) {
        for (int i=0; i<titleColors.count; i++) {
            [self setButtonTitleColor:[titleColors objectAtIndex:i] withState:*(stateArray + i)];
            if (i>=2) {
                break;
            }
        }
    }
    return self;
}

//0.06 设置TitleFont,
-(instancetype)setButtonFont:(CGFloat)fontSize{
    if (fontSize > 0) {
        self.titleLabel.font = FontCreate(fontSize);
    }
    return self;
}

//0.07 设置 父视图,
-(instancetype)setButtonSuperView:(UIView *)superView{
    [superView addSubview:self]  ;
    return self;
}

//0.08 设置 响应者 响应方法, （ 点击方法 ）
-(instancetype)setButtonTarget:(id)target andSelector:(SEL)selector
{
    if (target && selector) {
        [self addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

//0.09 设置title, 默认为normal
-(instancetype)setButtonTitle:(NSString *)title
{
    [self setButtonTitle:title withState:UIControlStateNormal];
    [self sizeToFit];
    return self;
}

//0.10 设置Image, 默认为normal
-(instancetype)setButtonImageName:(NSString *)imageName
{
    [self setButtonImageName:imageName withState:UIControlStateNormal];
    return self;
}

//0.11 设置BackgroundImages, 默认为normal
-(instancetype)setButtonBackgroundImageName:(NSString *)backgroundImageName
{
    [self setButtonBackgroundImageName:backgroundImageName withState:UIControlStateNormal];
    return self;
}

//0.12 设置BackgroundColor, 默认为normal
-(instancetype)setButtonBackgroundColor:(UIColor *)backgroundColor
{
    [self setButtonBackgroundColor:backgroundColor withState:UIControlStateNormal];
    return self;
}

//0.13 设置TitleColor, 默认为normal 
-(instancetype)setButtonTitleColor:(UIColor *)titleColor
{
    [self setButtonTitleColor:titleColor withState:UIControlStateNormal];
    return self;
}




/*
 
 +++++++> 类方法 <++++++
 
 */

//111>>========>> normal 状态

//1.00 设置normal -> titleFont
+(instancetype)buttonWithFont:(CGFloat)fontSize
{
    Class ButtonClass = [self class];
    UIButton *button = [[ButtonClass alloc] init];
    return [button setButtonFont:fontSize];
}

//1.01 设置normal -> 图片
+(instancetype)buttonWithImage:(NSString *)imageName
{
    UIButton *button = [[[self class] alloc] init];
    [button setButtonImageName:imageName withState:UIControlStateNormal];
    return button;
}

//1.02 设置normal -> title
+(instancetype)buttonWithTitle:(NSString *)title
{
    UIButton *button = [[[self class] alloc] init];
    [button setButtonTitle:title withState:UIControlStateNormal];
    [button sizeToFit];
    return button;
}

//1.03 设置normal -> title and titleColor
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor
{
    UIButton *button = [[[self class] alloc] init];
    
    [button setButtonTitle:title withState:UIControlStateNormal];
    [button setButtonTitleColor:titleColor withState:UIControlStateNormal];
    [button sizeToFit];
    
    return button;
}

//1.04 设置normal -> image and title
+(instancetype)buttonWithTitle:(NSString *)title andImageName:(NSString *)imageName
{
    UIButton *button = [[[self class] alloc] init];
    
    [button setButtonTitle:title withState:UIControlStateNormal];
    [button setButtonImageName:imageName withState:UIControlStateNormal];
    
    [button sizeToFit];
    return button;
}

//1.05 设置normal -> title / titleColor / image
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andImageName:(NSString *)imageName
{
    UIButton *button = [[[self class] alloc] init];
    
    [button setButtonTitle:title withState:UIControlStateNormal];
    [button setButtonTitleColor:titleColor withState:UIControlStateNormal];
    [button setButtonImageName:imageName withState:UIControlStateNormal];
    [button sizeToFit];
    
    return button;
}

//1.06 设置normal -> title / titleColor / BackgroundImage
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andBackgroundImageName:(NSString *)backgroundImageName
{
    UIButton *button = [[[self class] alloc] init];
    
    [button setButtonTitle:title withState:UIControlStateNormal];
    [button setButtonTitleColor:titleColor withState:UIControlStateNormal];
    [button setButtonBackgroundImageName:backgroundImageName withState:UIControlStateNormal];
    [button sizeToFit];
    
    return button;
}

//1.07 设置normal -> title / titleColor / BackgroundColor
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andBackgroundColor:(UIColor *)backgroundColor
{
    UIButton *button = [[[self class] alloc] init];
    
    [button setButtonTitle:title withState:UIControlStateNormal];
    [button setButtonTitleColor:titleColor withState:UIControlStateNormal];
    [button setButtonBackgroundColor:backgroundColor withState:UIControlStateNormal];
    [button sizeToFit];
    
    return button;
}

//1.08 设置normal -> title / titleColor / font
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andTitleFont:(CGFloat)fontSize
{
    UIButton *button = [[[self class] alloc] init];
    
    [button setButtonTitle:title withState:UIControlStateNormal];
    [button setButtonTitleColor:titleColor withState:UIControlStateNormal];
    [button setButtonFont:fontSize];
    [button sizeToFit];
    
    return button;
}

//1.09 设置normal -> title / titleColor / font /backgroundImage
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andTitleFont:(CGFloat)fontSize andBackgroundImageName:(NSString *)backgroundImageName
{
    UIButton *button = [[[self class] alloc] init];
    
    [button setButtonTitle:title withState:UIControlStateNormal];
    [button setButtonTitleColor:titleColor withState:UIControlStateNormal];
    [button setButtonBackgroundImageName:backgroundImageName withState:UIControlStateNormal];
    [button setButtonFont:fontSize];
    [button sizeToFit];
    
    return button;
}

//1.10 设置normal -> title / titleColor / font /backgroundColor
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andTitleFont:(CGFloat)fontSize andBackgroundColor:(UIColor *)backgroundColor
{
    UIButton *button = [[[self class] alloc] init];
    
    [button setButtonTitle:title withState:UIControlStateNormal];
    [button setButtonTitleColor:titleColor withState:UIControlStateNormal];
    [button setButtonBackgroundColor:backgroundColor withState:UIControlStateNormal];
    [button setButtonFont:fontSize];
    [button sizeToFit];
    
    return button;
}


//222 >>========>> normal select 状态

//2.01 设置normal<->selected ++> titles
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles
{
    Class ButtonClass = [self class];
    UIButton *button = [[ButtonClass alloc] init];
    return [button setButtonTitles:titles];
}

//2.02 设置normal<->selected ++> titles / titleColors
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)titleColors
{
    Class ButtonClass = [self class];
    UIButton *button = [[ButtonClass alloc] init];
    return [[button setButtonTitles:titles] setButtonTitleColors:titleColors];
}

//2.03 设置normal<->selected ++> titles / titleColors / fontSize
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)colors andTitleFont:(CGFloat)fontSize
{
    Class ButtonClass = [self class];
    UIButton *button = [[ButtonClass alloc] init];
    return [[[button setButtonFont:fontSize] setButtonTitleColors:colors] setButtonTitles:titles];
}

//2.04 设置normal<->selected ++> images
+(instancetype)buttonWithImageNames:(NSArray<NSString *> *)imageNames
{
    Class ButtonClass = [self class];
    UIButton *button = [[ButtonClass alloc] init];
    return [button setButtonImageNames:imageNames];
}

//2.05 设置normal<->selected ++> titles / images
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andImageNames:(NSArray<NSString *> *)imageNames
{
    return [[[self class] buttonWithImageNames:imageNames] setButtonTitles:titles];
}

//2.06 设置normal<->selected ++> titles / titleColors / images
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)titleColors andImageNames:(NSArray<NSString *> *)imageNames
{
    return [[[[self class] buttonWithImageNames:imageNames] setButtonTitles:titles] setButtonTitleColors:titleColors];
}

//2.07 设置normal<->selected ++> titles / titleColors / images / fontsize
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)titleColors andImageNames:(NSArray<NSString *> *)imageNames andTitleFont:(CGFloat)fontSize
{
    return [[self buttonWithTitles:titles andTitleColors:titleColors andTitleFont:fontSize] setButtonImageNames:imageNames];
}

//2.08 设置normal<->selected ++> titles / titleColors / BackgroundImageNames / fontsize
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)titleColors andBackgroundImageNames:(NSArray<NSString *> *)backgroundImageNames andTitleFont:(CGFloat)fontSize
{
    return [[self buttonWithTitles:titles andTitleColors:titleColors andTitleFont:fontSize] setButtonBackgroundImageNames:backgroundImageNames];
}

//2.09 设置normal<->selected ++> titles / titleColors / BackgroundColors / fontsize
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)titleColors andBackgroundColors:(NSArray<UIColor *> *)backgroundColors andTitleFont:(CGFloat)fontSize
{
    return [[self buttonWithTitles:titles andTitleColors:titleColors andTitleFont:fontSize] setButtonBackgroundColors:backgroundColors];
}


//333 >>========>> superView target SEL

//3.01 设置normal<->selected ++> titles / titleColors / fontsize /superView
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)titleColors andTitleFont:(CGFloat)fontSize andSuperView:(UIView *)superView
{
    return [[self buttonWithTitles:titles andTitleColors:titleColors andTitleFont:fontSize] setButtonSuperView:superView];
}

//3.02 设置normal -> title / titleColor / font /superView
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andTitleFont:(CGFloat)fontSize andSuperView:(UIView *)superView
{
    UIButton *button = [[[self class] alloc] init];
    
    [button setButtonTitle:title withState:UIControlStateNormal];
    [button setButtonTitleColor:titleColor withState:UIControlStateNormal];
    [[button setButtonFont:fontSize] setButtonSuperView:superView];
    [button sizeToFit];
    
    return button;
}

//3.03 设置normal<->selected ++> titles / titleColors / fontsize /superView /target /selector
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)titleColors andTitleFont:(CGFloat)fontSize andSuperView:(UIView *)superView andTarget:(id)target andSelector:(SEL)selector
{
    return [[[self buttonWithTitles:titles andTitleColors:titleColors andTitleFont:fontSize] setButtonSuperView:superView] setButtonTarget:target andSelector:selector];
}

//3.02 设置normal -> title / titleColor / font /superView
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andTitleFont:(CGFloat)fontSize andSuperView:(UIView *)superView andTarget:(id)target andSelector:(SEL)selector
{
    UIButton *button = [[[self class] alloc] init];
    
    [button setButtonTitle:title withState:UIControlStateNormal];
    [button setButtonTitleColor:titleColor withState:UIControlStateNormal];
    [[[button setButtonFont:fontSize] setButtonSuperView:superView] setButtonTarget:target andSelector:selector];
    [button sizeToFit];
    
    return button;
}


//add
+(instancetype)buttonWithImageName:(NSString *)imageName superView:(UIView *)superView target:(id)target selector:(SEL)method
{
    UIButton *btn = [UIButton buttonWithImageName:imageName target:target selector:method];
    [superView addSubview:btn];
    return btn;
}
+(instancetype)buttonWithImageName:(NSString *)imageName target:(id)target selector:(SEL)method
{
    UIButton *btn = [UIButton buttonWithImageName:imageName];
    [btn addTarget:target action:method forControlEvents:UIControlEventTouchUpInside];
    return btn;
}
+(instancetype)buttonWithImageName:(NSString *)imageName
{
    UIButton *btn = [[UIButton alloc]init];
    [btn setImage:ImageNamed(imageName) forState:UIControlStateNormal];
    [btn sizeToFit];
    return btn;
}
+(instancetype)buttonWithText:(NSString *)text color:(UIColor *)color fontStandSize:(CGFloat)size target:(id)target selector:(SEL)method
{
    UIButton *btn = [UIButton buttonWithText:text color:color fontStandSize:size];
    [btn addTarget:target action:method forControlEvents:UIControlEventTouchUpInside];
    return btn;
}
+(instancetype)buttonWithText:(NSString *)text color:(UIColor *)color fontStandSize:(CGFloat)size
{
    UIButton *btn = [[UIButton alloc]init];
    [btn setTitle:text forState:UIControlStateNormal];
    [btn setTitleColor:color forState:UIControlStateNormal];
    btn.titleLabel.font = UIFontFromStandSize(size);
    [btn sizeToFit];
    return btn;
}

@end
