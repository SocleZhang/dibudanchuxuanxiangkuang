//
//  AFYMethod.m
//  NewsTestDemo
//
//  Created by yzrs on 16/11/22.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import "AFYMethod.h"

#define len 30

@implementation AFYMethod

/* 计算文字空间 */
+ (CGSize)theFrameWithString:(NSString *)infoString andFontSize:(CGFloat)fontSize andWidth:(CGFloat)width
{
    if (infoString == nil || infoString.length == 0) {
        return CGSizeZero;
    }else{
        CGRect tempRect = [infoString boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]} context:nil];
        return tempRect.size;
    }
}
+ (CGSize)theFrameWithBoldString:(NSString *)infoString andFontSize:(CGFloat)fontSize andWidth:(CGFloat)width
{
    if (infoString == nil || infoString.length == 0) {
        return CGSizeZero;
    }else{
        CGRect tempRect = [infoString boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:fontSize]} context:nil];
        return tempRect.size;
    }
}
+ (CGSize)theFrameWithAttributedString:(NSMutableAttributedString *)infoString andWidth:(CGFloat)width
{
    if (infoString == nil || infoString.length == 0) {
        return CGSizeZero;
    }else{
        CGRect tempRect = [infoString boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:nil];
        return tempRect.size;
    }
}

//Alert提示

/* 正则表达式判断手机号 */
+ (BOOL)checkTelNumber:(NSString*) telNumber
{
    NSString *pattern = @"^1+[3578]+\\d{9}";
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pattern];
    
    BOOL isMatch = [pred evaluateWithObject:telNumber];
    
    return isMatch;
}
/* 密码数字字母组合（6-18位） */
+ (BOOL)checkPassword:(NSString*) password
{
    NSString *pattern = @"^(?![0-9]+$)(?![a-zA-Z]+$)[a-zA-Z0-9]{6,18}";
    
    NSPredicate*pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pattern];
    
    BOOL isMatch = [pred evaluateWithObject:password];
    
    return isMatch;
}

/* 地理坐标 转GEOHash */+(NSString *)geoHashWithLocation:(CLLocationCoordinate2D)coordinate{
    CLLocationDegrees latitude  = coordinate.latitude;  //纬度
    CLLocationDegrees longitude = coordinate.longitude; //经度
    
    NSString *resultLat = [self stringWithLatitude:latitude];
    NSString *resultLong= [self stringWithLongitude:longitude];
    
    //创建geo编码字符串
    NSMutableString *geoCode = [NSMutableString stringWithCapacity:(resultLat.length*2)];
    for (int i=0; i<resultLat.length; i++) {
        [geoCode appendString:[resultLong substringWithRange:NSMakeRange(i, 1)]];
        [geoCode appendString:[resultLat substringWithRange:NSMakeRange(i, 1)]];
    }
    
    //将字符串编码进行base32编码 ==》geo字符串
    NSMutableString *geoString = [NSMutableString stringWithCapacity:(geoCode.length/5)];
    NSRange geoRange = NSMakeRange(0, 5);
    while (geoRange.location + geoRange.length <= geoCode.length) {
        NSString *tempCode = [geoCode substringWithRange:geoRange];
        int geoValue = 0;
        for (int i=0; i<tempCode.length; i++) {
            int value = [[tempCode substringWithRange:NSMakeRange(i, 1)] intValue];
            geoValue = geoValue|(value << (tempCode.length - 1 -i));
        }
        [geoString appendString:[self base32codeWithValue:geoValue]];
        geoRange = NSMakeRange(geoRange.location + geoRange.length, geoRange.length);
    }
    
    //NSLog(@"%@",geoString);
    return geoString;
}


+(NSString*)stringWithLatitude:(CLLocationDegrees)latitude{
    
    CLLocationDegrees MaxLatitude = 90; //最大纬度
    CLLocationDegrees MinLatitude = -90;//最小纬度
    CLLocationDegrees MidLatitude = (MaxLatitude + MinLatitude)/2;//中间纬度
    CLLocationDegrees ValLatitude,ErrLatitude = 45;//下一次的平均纬度、纬度误差
    NSInteger times = len;
    //00000000 00000000 00000000 00000000
    NSMutableString *result = [NSMutableString stringWithCapacity:times];
    
    CLLocationDegrees temp = latitude;
    
    while (ErrLatitude > 0.001 && times != 0) {
        
        if (temp >= MidLatitude && temp <= MaxLatitude) {
            ValLatitude = (MidLatitude + MaxLatitude)/2;
            MinLatitude = MidLatitude;
            [result appendString:@"1"];
        }else{
            ValLatitude = (MidLatitude + MinLatitude)/2;
            MaxLatitude = MidLatitude;
            [result appendString:@"0"];
        }
        //忽略纬度误差 取times
        //ErrLatitude = fabsl(ValLatitude - MidLatitude);
        
        MidLatitude = ValLatitude;
        
        times--;
    }
    
    while (result.length < len) {
        [result appendString:@"0"];
    }
    
    return result;
}

+(NSString*)stringWithLongitude:(CLLocationDegrees)longitude{
    
    CLLocationDegrees MaxLongitude = 180; //最大经度
    CLLocationDegrees MinLongitude = -180;//最小经度
    CLLocationDegrees MidLongitude = (MaxLongitude + MinLongitude)/2;//中间纬度
    CLLocationDegrees ValLongitude,ErrLongitude = 90;//下一次的平均经度、经度误差
    NSInteger times = len;
    //00000000 00000000 00000000 00000000
    NSMutableString *result = [NSMutableString stringWithCapacity:times];
    
    CLLocationDegrees temp = longitude;
    
    while (ErrLongitude > 0.001 && times != 0) {
        
        if (temp >= MidLongitude && temp <= MaxLongitude) {
            ValLongitude = (MidLongitude + MaxLongitude)/2;
            MinLongitude = MidLongitude;
            [result appendString:@"1"];
        }else{
            ValLongitude = (MidLongitude + MinLongitude)/2;
            MaxLongitude = MidLongitude;
            [result appendString:@"0"];
        }
        //忽略误差，取30次
        //ErrLongitude = fabsl(ValLongitude - MidLongitude);
        
        MidLongitude = ValLongitude;
        
        times--;
    }
    
    while (result.length < len) {
        [result appendString:@"0"];
    }
    
    return result;
}

+(NSString *)base32codeWithValue:(int)value{
    if (value <= 9) {
        return [NSString stringWithFormat:@"%d",value];
    }else if (value <= 16){
        return [NSString stringWithFormat:@"%c",('b' + value - 10)];
    }else if (value <= 18){
        return [NSString stringWithFormat:@"%c",('b' + value - 10 + 1)];
    }else if (value <= 20){
        return [NSString stringWithFormat:@"%c",('b' + value - 10 + 2)];
    }else {
        return [NSString stringWithFormat:@"%c",('b' + value - 10 + 3)];
    }
}


+(CLLocationCoordinate2D)theLocationWithGEOHashString:(NSString *)geoHash{
    CLLocationCoordinate2D coordinate;
    
    NSMutableString *geoString = [NSMutableString string];
    
    NSMutableString *latitudeString   = [NSMutableString string];
    NSMutableString *longtitudeString = [NSMutableString string];
    
    for (int i=0; i<geoHash.length; i++) {
        NSString *geoCh = [geoHash substringWithRange:NSMakeRange(i, 1)];
        [geoString appendString:[self getCodeStringByChar:geoCh]];
    }
    
    for (int i=0; i<geoString.length; i++) {
        NSString *single = [geoString substringWithRange:NSMakeRange(i, 1)];
        if (i%2 == 0) {
            [longtitudeString appendString:single];//纬度
        }else{
            [latitudeString appendString:single];//精度
        }
    }
    
    coordinate.latitude  = [self latitudeByString:latitudeString];
    coordinate.longitude = [self longitudeByString:longtitudeString];
    
    return coordinate;
}


+(CLLocationDegrees)latitudeByString:(NSString *)latString{
    CLLocationDegrees lati = 0;
    CLLocationDegrees Max = 0;
    CLLocationDegrees Min = 0;
    for (int i=0; i<latString.length; i++) {
        int value = [[latString substringWithRange:NSMakeRange(i, 1)] intValue];
        if(i == 0){
            if (value == 1) {
                Max = 90;
                Min = 0;
            }else{
                Max = 0;
                Min = -90;
            }
        }else{
            if (value == 1) {
                Min = lati;
            }else{
                Max = lati;
            }
        }
        
        lati = (Max + Min)/2;
    }
    
    return lati;
}

+(CLLocationDegrees)longitudeByString:(NSString *)longString{
    CLLocationDegrees longi = 0;
    CLLocationDegrees Max = 0;
    CLLocationDegrees Min = 0;
    for (int i=0; i<longString.length; i++) {
        int value = [[longString substringWithRange:NSMakeRange(i, 1)] intValue];
        if(i == 0){
            if (value == 1) {
                Max = 180;
                Min = 0;
            }else{
                Max = 0;
                Min = -180;
            }
        }else{
            if (value == 1) {
                Min = longi;
            }else{
                Max = longi;
            }
        }
        
        longi = (Max + Min)/2;
    }
    
    return longi;
}

+(NSString *)getCodeStringByChar:(NSString *)ch{
    NSDictionary *dictionary = @{@"0":@"00000",//0
                                 @"1":@"00001",//1
                                 @"2":@"00010",//2
                                 @"3":@"00011",//3
                                 @"4":@"00100",//4
                                 @"5":@"00101",//5
                                 @"6":@"00110",//6
                                 @"7":@"00111",//7
                                 @"8":@"01000",//8
                                 @"9":@"01001",//9
                                 @"b":@"01010",//10
                                 @"c":@"01011",//11
                                 @"d":@"01100",//12
                                 @"e":@"01101",//13
                                 @"f":@"01110",//14
                                 @"g":@"01111",//15
                                 @"h":@"10000",//16
                                 @"j":@"10001",//17
                                 @"k":@"10010",//18
                                 @"m":@"10011",//19
                                 @"n":@"10100",//20
                                 @"p":@"10101",//21
                                 @"q":@"10110",//22
                                 @"r":@"10111",//23
                                 @"s":@"11000",//24
                                 @"t":@"11001",//25
                                 @"u":@"11010",//26
                                 @"v":@"11011",//27
                                 @"w":@"11100",//28
                                 @"x":@"11101",//29
                                 @"y":@"11110",//30
                                 @"z":@"11111",//31
                                 };
    
    return dictionary[ch];
}


//把火星坐标转换成百度坐标
+(CLLocationCoordinate2D)bd_encryptWtithCoordinate:(CLLocationCoordinate2D)coord{
    CLLocationCoordinate2D  bd_enCoord;
    
    double x = coord.longitude;
    double y = coord.latitude;
    
    double z = sqrt(x * x + y * y) + 0.00002 * sin(y * M_PI);
    double theta = atan2(y, x) + 0.000003 * cos(x * M_PI);
    
    bd_enCoord.longitude   = z * cos(theta) + 0.0065;
    bd_enCoord.latitude  = z * sin(theta) + 0.006;
    
    return bd_enCoord;
}
//百度坐标转换成火星坐标
+(CLLocationCoordinate2D)bd_decryptWtithCoordinate:(CLLocationCoordinate2D)coord{
    CLLocationCoordinate2D  bd_deCoord;
    
    double x = coord.longitude - 0.0065, y = coord.latitude - 0.006;
    double z = sqrt(x * x + y * y) - 0.00002 * sin(y * M_PI);
    double theta = atan2(y, x) - 0.000003 * cos(x * M_PI);
    
    bd_deCoord.latitude   = z * sin(theta);
    bd_deCoord.longitude  = z * cos(theta);
    
    return bd_deCoord;
}



//颜色转为图片
+(UIImage*)createImageWithColor:(UIColor*)color
{
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

@end
