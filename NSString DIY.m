//
//  NSString+DIY.m
//  NewsTestDemo
//
//  Created by yzrs on 16/11/22.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import "NSString+DIY.h"

@implementation NSString (DIY)

/* hash String */
-(NSString *)hashString{
    return [NSString stringWithFormat:@"%lu", self.hash];
}

//1.0 计算文字空间大小
// 001
-(CGFloat)getStringWidthByFontSize:(CGFloat)fontSize
{
    CGRect tempRect = [self boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]} context:nil];
    
    return tempRect.size.width;
}

-(CGFloat)getStringWidthByBoldFontSize:(CGFloat)boldFontSize;
{
    CGRect tempRect = [self boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:boldFontSize]} context:nil];
    
    return tempRect.size.width;
}

// 002
-(CGFloat)getStringHeightByFontSize:(CGFloat)fontSize andMaxWidth:(CGFloat)maxWidth
{
    CGSize tempSize = [self getStringSizeByFontSize:fontSize andMaxWidth:maxWidth];
    return tempSize.height;
}
-(CGFloat)getStringHeightByBoldFontSize:(CGFloat)boldFontSize andMaxWidth:(CGFloat)maxWidth
{
    CGSize tempSize = [self getStringSizeByBoldFontSize:boldFontSize andMaxWidth:maxWidth];
    return tempSize.height;
}

// 003
-(CGSize)getStringSizeByFontSize:(CGFloat)fontSize andMaxWidth:(CGFloat)maxWidth
{
    if (maxWidth == 0) {
        maxWidth = CGFLOAT_MAX;
    }
    CGSize tempSize = [AFYMethod theFrameWithString:self andFontSize:fontSize andWidth:maxWidth];
    
    return tempSize;
}
-(CGSize)getStringSizeByBoldFontSize:(CGFloat)boldFontSize andMaxWidth:(CGFloat)maxWidth
{
    if (maxWidth == 0) {
        maxWidth = CGFLOAT_MAX;
    }
    CGSize tempSize = [AFYMethod theFrameWithBoldString:self andFontSize:boldFontSize andWidth:maxWidth];
    
    return tempSize;
}


//小数转字符串
+(NSString *)stringByDouble:(double)doubleValue
{
    NSString *string = [NSString stringWithFormat:@"%.2lf", doubleValue];
    
    NSArray *strArr = [string componentsSeparatedByString:@"."];
    //小数点后为00
    if ([strArr.lastObject integerValue] == 0) {
        return strArr.firstObject;
    }
    //小数点后 最后位为0
    if ([[strArr.lastObject substringFromIndex:1] integerValue] == 0) {
        return [NSString stringWithFormat:@"%.1lf", doubleValue];
    }
    return string;
}

@end
