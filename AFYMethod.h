//
//  AFYMethod.h
//  NewsTestDemo
//
//  Created by yzrs on 16/11/22.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "UIView+DIY.h"

// rgb颜色转换（16进制->10进制）
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define FontCreate(fontSize) [UIFont systemFontOfSize:fontSize]

//创建图片
#define ImageCreate(imageName) [UIImage imageNamed:imageName]

@interface AFYMethod : NSObject

/* 计算文字空间 */
+ (CGSize)theFrameWithString:(NSString *)infoString andFontSize:(CGFloat)fontSize andWidth:(CGFloat)width;
+ (CGSize)theFrameWithBoldString:(NSString *)infoString andFontSize:(CGFloat)fontSize andWidth:(CGFloat)width;
+ (CGSize)theFrameWithAttributedString:(NSMutableAttributedString *)infoString andWidth:(CGFloat)width;

//Alert提示

/* 正则表达式判断手机号 */
+ (BOOL)checkTelNumber:(NSString*) telNumber;
/* 密码数字字母组合（6-18位） */
+ (BOOL)checkPassword:(NSString*) password;

/* 地理坐标 转GEOHash */
+ (NSString *)geoHashWithLocation:(CLLocationCoordinate2D)coordinate;
+(CLLocationCoordinate2D)theLocationWithGEOHashString:(NSString *)geoHash;

//把火星坐标转换成百度坐标
+(CLLocationCoordinate2D)bd_encryptWtithCoordinate:(CLLocationCoordinate2D)coord;
//百度坐标转换成火星坐标
+(CLLocationCoordinate2D)bd_decryptWtithCoordinate:(CLLocationCoordinate2D)coord;

//颜色转为图片
+(UIImage*)createImageWithColor:(UIColor*)color;

@end
