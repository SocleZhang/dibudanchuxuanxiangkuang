//
//  AFYFileManage.h
//  NewsTestDemo
//
//  Created by yzrs on 16/11/28.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFYFileManage : NSObject

+(instancetype)shareAfyFileManager;

@property(nonatomic,copy  )NSString *afyCachePath;//缓存图片

@property(nonatomic,copy  )NSString *afyCollectPath;//收藏图片
@property(nonatomic,copy  )NSString *afyCollectDict;//收藏图片的顺序数组


/* 1.0 缓存 */
//1.1 缓存数据
-(BOOL)saveCacheImageWithData:(NSData *)data forKey:(NSString *)key;
//1.2 清除缓存
-(BOOL)clearCacheImageData;
//1.3 返回缓存数据
-(NSDictionary *)isCacheFileExistByTheKey:(NSString *)key;

/* 2.0 收藏*/

//2.1 添加收藏
-(BOOL)addToCollectDirWithData:(NSData *)data forKey:(NSString *)key;

-(BOOL)savedCollectOrderWithPath:(NSString *)key;

-(NSArray *)getCollectArray;

//2.2 返回收藏 数据
-(NSArray *)getTheCollectionData;

//删除收藏数据
-(BOOL)deletCollectDataByTheKey:(NSString *)key;

@end
