//
//  NSString+DIY.h
//  NewsTestDemo
//
//  Created by yzrs on 16/11/22.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFYMethod.h"

@interface NSString (DIY)

@property(nonatomic,copy)NSString *hashString;

//1.0 计算文字空间大小
-(CGFloat)getStringWidthByFontSize:(CGFloat)fontSize;
-(CGFloat)getStringWidthByBoldFontSize:(CGFloat)boldFontSize;

-(CGFloat)getStringHeightByFontSize:(CGFloat)fontSize andMaxWidth:(CGFloat)maxWidth;
-(CGFloat)getStringHeightByBoldFontSize:(CGFloat)boldFontSize andMaxWidth:(CGFloat)maxWidth;

-(CGSize)getStringSizeByFontSize:(CGFloat)fontSize andMaxWidth:(CGFloat)maxWidth;
-(CGSize)getStringSizeByBoldFontSize:(CGFloat)boldFontSize andMaxWidth:(CGFloat)maxWidth;


//小数转字符串
+(NSString *)stringByDouble:(double)doubleValue;

@end
