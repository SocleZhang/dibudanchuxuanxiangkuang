//
//  UILabel+DIY.h
//  NewsTestDemo
//
//  Created by yzrs on 16/11/21.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFYMethod.h"

@interface UILabel (DIY)

//frame属性可至UIView+DIY处查看
//【注:需要先alloc init 初始化后使用 】 设置label的方法
-(instancetype)labelWithColor:(UIColor *)color;
-(instancetype)labelWithFontSize:(CGFloat)fontSize;
-(instancetype)labelWithBoldFontSize:(CGFloat)fontSize;
-(instancetype)labelWithText:(NSString *)text;
-(instancetype)labelWithSuperView:(UIView *)superView;

//以下为 快速设置UILabel的类方法
//初始化 赋值 颜色  父视图
+(instancetype)labelWithFontSize:(CGFloat)fontSize andColor:(UIColor *)color andText:(NSString *)text;
+(instancetype)labelWithFontSize:(CGFloat)fontSize andColor:(UIColor *)color andText:(NSString *)text andSuperView:(UIView *)superView;

//初始化 赋值 颜色  父视图 【粗体】
+(instancetype)labelWithBoldFontSize:(CGFloat)fontSize andColor:(UIColor *)color andText:(NSString *)text;
+(instancetype)labelWithBoldFontSize:(CGFloat)fontSize andColor:(UIColor *)color andText:(NSString *)text andSuperView:(UIView *)superView;

@end
