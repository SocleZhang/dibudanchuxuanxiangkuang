//
//  NSObject+DIY.h
//  NewsTestDemo
//
//  Created by yzrs on 16/11/22.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AFYMethod.h"

@interface NSObject (DIY)

/* 返回该对象的字符串方法 */
@property (nonatomic, copy, readonly) NSString *afy_string;

/* 对象的所有属性和值 */
- (NSDictionary *)getAllPropertiesAndVaules;

#pragma mark - 拨打电话
-(void)callServiceWithTel:(NSString *)telString withSuperView:(UIView *)superView;

@end
