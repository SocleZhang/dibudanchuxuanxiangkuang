//
//  UIView+DIY.h
//  NewsTestDemo
//
//  Created by yzrs on 16/11/21.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - 默认颜色 可修改
#define DefaultColor UIColorFromRGB(0x909090) //浅灰色

//定义一个Block
typedef void(^FrameBlock)(UIView *view);

@interface UIView (DIY)

/* 1. 定义部分属性 */
@property(nonatomic,assign)CGFloat      afy_topY;       //frame.origin.y
@property(nonatomic,assign)CGFloat      afy_leftX;      //frame.origin.x
@property(nonatomic,assign)CGFloat      afy_bottomY;    //frame.origin.y + frame.size.height
@property(nonatomic,assign)CGFloat      afy_rightX;     //frame.origin.x + frame.size.width
@property(nonatomic,assign)CGFloat      afy_width;      //frame.size.width
@property(nonatomic,assign)CGFloat      afy_height;     //frame.size.height

@property(nonatomic,assign)CGFloat      afy_centerX;    //frame.center.x
@property(nonatomic,assign)CGFloat      afy_centerY;    //frame.center.y

@property(nonatomic,assign)CGPoint      afy_origin;     //frame.origin
@property(nonatomic,assign)CGSize       afy_size;       //frame.size

/* 2. 快速初始化UIView 等*/

//2.1  用frameBlock 设置View的frame等属性
-(instancetype)setFrameBlock:(FrameBlock)frameBlock;
//2.2  view添加tag
-(instancetype)addTag:(NSInteger)tag;
//2.3  view添加到superView
-(instancetype)addToTheSuperView:(UIView *)superView;
//2.4  view添加手势
-(instancetype)addTapGuestureTarget:(id)target withSelector:(SEL)selector;

//2.5  类方法创建view 并用frameBlock 设置view的frame等属性
+(instancetype)viewWithFrameBlock:(FrameBlock)frameBlock;
//2.6  类方法创建view 并添加到父视图
+(instancetype)viewWithSuperView:(UIView *)superView;
//2.7  类方法创建view 设置背景颜色  并添加到父视图
+(instancetype)viewWithSuperView:(UIView *)superView andBgColor:(UIColor *)bgColor;
//2.8  类方法创建view 用frameBlock 设置view的frame等属性 并添加至父视图
+(instancetype)viewWithSuperView:(UIView *)superView andFrameBlock:(FrameBlock)frameBlock;
//2.9  类方法创建view 设置背景颜色 用frameBlock 设置view的frame等属性 并添加至父视图
+(instancetype)viewWithSuperView:(UIView *)superView andBgColor:(UIColor *)bgColor andFrameBlock:(FrameBlock)frameBlock;


/* 3. 添加线条 */
//默认为view的宽度  高度为0.5
//添加线条。默认颜色为 DefaultColor  可至本页前面修改
-(instancetype)addTopLine;
-(instancetype)addBottomLine;
-(instancetype)addTopAndBottomLine;

-(instancetype)addTopLineWithColor:(UIColor *)color;
-(instancetype)addBottomLineWithColor:(UIColor *)color;
-(instancetype)addTopAndBottomLineWithColor:(UIColor *)color;

-(instancetype)addLineWithFrameBlock:(FrameBlock)frameBlock;
-(instancetype)addLineWithColor:(UIColor *)color andFrameBlock:(FrameBlock)frameBlock;

@end
