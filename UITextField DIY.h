//
//  UITextField+DIY.h
//  NewsTestDemo
//
//  Created by yzrs on 16/11/22.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFYMethod.h"

@interface UITextField (DIY)

+(instancetype)textFieldWithStandFontSize:(CGFloat)size placeHolderText:(NSString *)text placeHolderColor:(UIColor *)color;

+(instancetype)textFieldWithStandFontSize:(CGFloat)size placeHolderText:(NSString *)text placeHolderColor:(UIColor *)color superView:(UIView *)superView;

@end
