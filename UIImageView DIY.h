//
//  UIImageView+DIY.h
//  NewsTestDemo
//
//  Created by yzrs on 16/11/22.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFYMethod.h"

@interface UIImageView (DIY)

//快速创建 UIImageView  类方法
+(instancetype)imageViewWithImageName:(NSString *)imageName;
+(instancetype)imageViewWithImageName:(NSString *)imageName andSuperView:(UIView *)superView;

+(instancetype)imageViewWithImageName:(NSString *)imageName andTarget:(id)target andSelector:(SEL)selector;
+(instancetype)imageViewWithImageName:(NSString *)imageName andTarget:(id)target andSelector:(SEL)selector andSuperView:(UIView *)superView;

@end
