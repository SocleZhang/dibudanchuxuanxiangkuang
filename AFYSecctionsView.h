//
//  AFYSecctionsView.h
//  AFYTestProduct
//
//      /******** 描述文件 *********/
//          多项选项 选择界面>>>
//          如:照片-->1.从相册中选择 2.相机拍照
//          等
//      /*************************/
//
//  Created by yzrs on 16/12/8.
//  Copyright © 2016年 ArtemisFang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFYFunction.h"

#define TheAFYWidth  [UIScreen mainScreen].bounds.size.width

#define TheAFYHeight [UIScreen mainScreen].bounds.size.height

/* cell 高度 */
#define AFYSecctionViewCellHeight 50
/* cell 分组间隔 */
#define AFYSecctionViewCellGap     5

/* title 字体默认大小 */
#define AFYSecctionTitleFont       [UIFont systemFontOfSize:18]
/* title 字体默认颜色 */
#define AFYSecctionTitleColor      UIColorFromRGB(0x090909)

/* message 字体默认大小 */
#define AFYSecctionMessageFont     [UIFont systemFontOfSize:10]
/* message 字体默认颜色 */
#define AFYSecctionMessageColor    UIColorFromRGB(0x323232)

/* cell 字体默认大小 */
#define AFYSecctionTextFont       [UIFont systemFontOfSize:15]
/* cell 字体默认颜色 */
#define AFYSecctionTextColor      UIColorFromRGB(0x090909)
/* 动画显示时间 */
#define AFYAnimationTime          0.3

typedef NS_ENUM(NSInteger, AFYSecctionViewStyle) {
    AFYSecctionViewStyleDefalt = 0,
    AFYSecctionViewStyleSystem,
};

typedef NS_ENUM(NSInteger, AFYSecctionType) {
    AFYSecctionViewTypeDefault = 0,
    AFYSecctionViewTypeCancel,
    AFYSecctionViewTypeDestructive,
};


@class AFYSecctionsView;

//AFYSecctionsView  代理
@protocol AFYSecctionsViewDelegate <NSObject>

@optional
-(void)secctionView:(AFYSecctionsView *)secctionView selectTitleWithIndexPath:(NSIndexPath *)indexPath;

@end



@interface AFYSecctionsView : UIView

@property(nonatomic,strong)NSDictionary *specitalCellTextColorInfo;//@{@"Text":UIColor}

/* 1.0 title */
@property(nonatomic,copy  )NSString       *title;//选项 提示标题

@property(nonatomic,strong)UIFont  *titleFont;

@property(nonatomic,strong)UIColor *titleColor;

/* 2.0 message */
@property(nonatomic,copy  )NSString       *message;//选项 提示信息

@property(nonatomic,strong)UIFont  *messageFont;

@property(nonatomic,strong)UIColor *messageColor;

/* 3.0 secctionTexts */
@property(nonatomic,strong)NSArray *secctionTexts;//选择项 字符串 数组

@property(nonatomic,strong)UIFont  *secctionTextFont;

@property(nonatomic,strong)UIColor *secctionTextColor;

/* 4.0 代理 */
@property(nonatomic,weak  )id<AFYSecctionsViewDelegate> delegate;

/* 5.0 选项高度 */
@property(nonatomic,assign)CGFloat secctionHeight;

//选项视图风格
@property(nonatomic,assign)AFYSecctionViewStyle secctionStyle;

//创建 选择项view 默认字体和高宽
-(instancetype)initWithTitle:(NSString *)title message:(NSString *)message secctionTexts:(NSArray *)secctionTexts andDelegate:(id<AFYSecctionsViewDelegate>)delegate;

//创建 选择项view 给出字体和高宽
-(instancetype)initWithTitle:(NSString *)title message:(NSString *)message secctionTexts:(NSArray *)secctionTexts andTextFont:(CGFloat)textFont andTextColor:(UIColor *)textColor andDelegate:(id<AFYSecctionsViewDelegate>)delegate;

/* 创建UI 并显示 */
-(void)show;//显示选项框

@end






@protocol AFYSecctionsButtonDelegate <NSObject>

-(void)secctionButton:(UIButton *)button withIndexPath:(NSIndexPath *)indexPath;

@end

@interface AFYSecctionsButton : UIButton

@property(nonatomic,strong)NSIndexPath *indexPath;

@property(nonatomic,weak  )id<AFYSecctionsButtonDelegate> delegate;

@property(nonatomic,strong)UIView      *backgroundView01;
@property(nonatomic,strong)UIView      *backgroundView02;

@end









