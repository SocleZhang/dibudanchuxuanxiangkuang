//
//  UIImageView+DIY.m
//  NewsTestDemo
//
//  Created by yzrs on 16/11/22.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import "UIImageView+DIY.h"

@implementation UIImageView (DIY)

//快速创建 UIImageView  类方法
+(instancetype)imageViewWithImageName:(NSString *)imageName
{
    UIImageView *imageView = [[[self class] alloc] initWithImage:ImageCreate(imageName)];
    return imageView;
}
+(instancetype)imageViewWithImageName:(NSString *)imageName andSuperView:(UIView *)superView
{
    UIImageView *imageView = [[[self class] alloc] initWithImage:ImageCreate(imageName)];
    [superView addSubview:imageView];
    return imageView;
}

+(instancetype)imageViewWithImageName:(NSString *)imageName andTarget:(id)target andSelector:(SEL)selector
{
    UIImageView *imageView = [[[self class] alloc] initWithImage:ImageCreate(imageName)];
    imageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:target action:selector];
    [imageView addGestureRecognizer:tap];
    
    return imageView;
}
+(instancetype)imageViewWithImageName:(NSString *)imageName andTarget:(id)target andSelector:(SEL)selector andSuperView:(UIView *)superView
{
    UIImageView *imageView = [[[self class] alloc] initWithImage:ImageCreate(imageName)];
    imageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:target action:selector];
    [imageView addGestureRecognizer:tap];
    
    return imageView;
}

@end
