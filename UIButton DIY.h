//
//  UIButton+DIY.h
//  NewsTestDemo
//
//  Created by yzrs on 16/11/22.
//  Copyright © 2016年 方媛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFYMethod.h"

@interface UIButton (DIY)

//0.01 设置titles, 默认第一个为normal 第二个为select
-(instancetype)setButtonTitles:(NSArray<NSString *> *)titleArray;

//0.02 设置Images, 默认第一个为normal 第二个为select
-(instancetype)setButtonImageNames:(NSArray<NSString *> *)imageNames;

//0.03 设置BackgroundImages, 默认第一个为normal 第二个为select
-(instancetype)setButtonBackgroundImageNames:(NSArray<NSString *> *)backgroundImageNames;

//0.04 设置BackgroundColor, 默认第一个为normal 第二个为select
-(instancetype)setButtonBackgroundColors:(NSArray<UIColor *> *)backgroundColors;

//0.05 设置TitleColor, 默认第一个为normal 第二个为select
-(instancetype)setButtonTitleColors:(NSArray<UIColor *> *)titleColors;

//0.06 设置TitleFont,
-(instancetype)setButtonFont:(CGFloat)fontSize;

//0.07 设置 父视图,
-(instancetype)setButtonSuperView:(UIView *)superView;

//0.08 设置 响应者 响应方法, （ 点击方法 ）
-(instancetype)setButtonTarget:(id)target andSelector:(SEL)selector;

//0.09 设置title, 默认为normal
-(instancetype)setButtonTitle:(NSString *)title;

//0.10 设置Image, 默认为normal
-(instancetype)setButtonImageName:(NSString *)imageName;

//0.11 设置BackgroundImages, 默认为normal
-(instancetype)setButtonBackgroundImageName:(NSString *)backgroundImageName;

//0.12 设置BackgroundColor, 默认为normal
-(instancetype)setButtonBackgroundColor:(UIColor *)backgroundColor;

//0.13 设置TitleColor, 默认为normal 
-(instancetype)setButtonTitleColor:(UIColor *)titleColor;

/*
 
 +++++++> 类方法 <++++++
 
 */

//111>>========>> normal 状态

//1.00 设置normal -> titleFont
+(instancetype)buttonWithFont:(CGFloat)fontSize;

//1.01 设置normal -> 图片
+(instancetype)buttonWithImage:(NSString *)imageName;

//1.02 设置normal -> title
+(instancetype)buttonWithTitle:(NSString *)title;

//1.03 设置normal -> title and titleColor
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor;

//1.04 设置normal -> image and title
+(instancetype)buttonWithTitle:(NSString *)title andImageName:(NSString *)imageName;

//1.05 设置normal -> title / titleColor / image
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andImageName:(NSString *)imageName;

//1.06 设置normal -> title / titleColor / BackgroundImage
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andBackgroundImageName:(NSString *)backgroundImageName;

//1.07 设置normal -> title / titleColor / BackgroundColor
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andBackgroundColor:(UIColor *)backgroundColor;

//1.08 设置normal -> title / titleColor / font
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andTitleFont:(CGFloat)fontSize;

//1.09 设置normal -> title / titleColor / font /backgroundImage
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andTitleFont:(CGFloat)fontSize andBackgroundImageName:(NSString *)backgroundImageName;

//1.10 设置normal -> title / titleColor / font /backgroundColor
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andTitleFont:(CGFloat)fontSize andBackgroundColor:(UIColor *)backgroundColor;


//222 >>========>> normal select 状态

//2.01 设置normal<->selected ++> titles
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles;

//2.02 设置normal<->selected ++> titles / titleColors
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)titleColors;

//2.03 设置normal<->selected ++> titles / titleColors / fontSize
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)colors andTitleFont:(CGFloat)fontSize;

//2.04 设置normal<->selected ++> images
+(instancetype)buttonWithImageNames:(NSArray<NSString *> *)imageNames;

//2.05 设置normal<->selected ++> titles / images
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andImageNames:(NSArray<NSString *> *)imageNames;

//2.06 设置normal<->selected ++> titles / titleColors / images
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)titleColors andImageNames:(NSArray<NSString *> *)imageNames;

//2.07 设置normal<->selected ++> titles / titleColors / images / fontsize
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)titleColors andImageNames:(NSArray<NSString *> *)imageNames andTitleFont:(CGFloat)fontSize;

//2.08 设置normal<->selected ++> titles / titleColors / BackgroundImageNames / fontsize
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)titleColors andBackgroundImageNames:(NSArray<NSString *> *)backgroundImageNames andTitleFont:(CGFloat)fontSize;

//2.09 设置normal<->selected ++> titles / titleColors / BackgroundColors / fontsize
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)titleColors andBackgroundColors:(NSArray<UIColor *> *)backgroundColors andTitleFont:(CGFloat)fontSize;


//333 >>========>> superView target SEL

//3.01 设置normal<->selected ++> titles / titleColors / fontsize /superView
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)titleColors andTitleFont:(CGFloat)fontSize andSuperView:(UIView *)superView;

//3.02 设置normal -> title / titleColor / font /superView
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andTitleFont:(CGFloat)fontSize andSuperView:(UIView *)superView;

//3.03 设置normal<->selected ++> titles / titleColors / fontsize /superView /target /selector
+(instancetype)buttonWithTitles:(NSArray<NSString *> *)titles andTitleColors:(NSArray<UIColor *> *)titleColors andTitleFont:(CGFloat)fontSize andSuperView:(UIView *)superView andTarget:(id)target andSelector:(SEL)selector;

//3.02 设置normal -> title / titleColor / font /superView
+(instancetype)buttonWithTitle:(NSString *)title andTitleColor:(UIColor *)titleColor andTitleFont:(CGFloat)fontSize andSuperView:(UIView *)superView andTarget:(id)target andSelector:(SEL)selector;

//add
+(instancetype)buttonWithImageName:(NSString *)imageName superView:(UIView *)superView target:(id)target selector:(SEL)method;
+(instancetype)buttonWithImageName:(NSString *)imageName target:(id)target selector:(SEL)method;
+(instancetype)buttonWithImageName:(NSString *)imageName;
+(instancetype)buttonWithText:(NSString *)text color:(UIColor *)color fontStandSize:(CGFloat)size target:(id)target selector:(SEL)method;
+(instancetype)buttonWithText:(NSString *)text color:(UIColor *)color fontStandSize:(CGFloat)size;

@end
