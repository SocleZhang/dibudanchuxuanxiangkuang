//
//  AFYFunction.h
//  NewsTestDemo
//
//  Created by yzrs on 16/11/21.
//  Copyright © 2016年 方媛. All rights reserved.
//

#ifndef AFYFunction_h
#define AFYFunction_h

#endif /* AFYFunction_h */

#import "UIView+DIY.h"
#import "UILabel+DIY.h"
#import "UIImageView+DIY.h"
#import "UIButton+DIY.h"
#import "UITextField+DIY.h"
#import "NSObject+DIY.h"
#import "NSString+DIY.h"
#import "AFYMethod.h"

#import "AFYFileManage.h"

